/*
 * @Author: hongbin
 * @Date: 2022-05-17 18:21:45
 * @LastEditors: hongbin
 * @LastEditTime: 2022-05-19 14:39:08
 * @Description: 需要插入到页面的代码 需要更新至需要在服务端更改 不需要更新插件
 */

const stylePrefix = 'hongbin666-';
function createDom(tag, className) {
  const dom = window.top.document.createElement(tag);
  if (className) dom.className = stylePrefix + className;
  return dom;
}

function createButton(text, className, callback) {
  const dom = document.createElement('button');
  if (className) dom.className = stylePrefix + className;
  dom.innerHTML = text;
  dom.onclick = callback;
  return dom;
}


/**
* 载入 图片裁剪插件 js & css
*/

const script = createDom('script');
// script.src = "https://api.hongbin.xyz:3002/kmyc/cropperjs.min.js";
script.src = "https://api.hongbin.xyz:3002/kmyc/cropperjs.js";
// script.src = "./cropperjs.js";

script.addEventListener("load", e => {
  log(e);
})

const cropperStyle = createDom('style');

cropperStyle.innerHTML = `
/*!
 * Cropper.js v1.5.12
 * https://fengyuanchen.github.io/cropperjs
 *
 * Copyright 2015-present Chen Fengyuan
 * Released under the MIT license
 *
 * Date: 2021-06-12T08:00:11.623Z
 */
.cropper-container {
  direction: ltr;
  font-size: 0;
  line-height: 0;
  position: relative;
  -ms-touch-action: none;
  touch-action: none;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}
.cropper-container img {
  image-orientation: 0deg;
  display: block;
  height: 100%;
  max-height: none !important;
  max-width: none !important;
  min-height: 0 !important;
  min-width: 0 !important;
  width: 100%;
}
.cropper-canvas,
.cropper-crop-box,
.cropper-drag-box,
.cropper-modal,
.cropper-wrap-box {
  bottom: 0;
  left: 0;
  position: absolute;
  right: 0;
  top: 0;
}
.cropper-canvas,
.cropper-wrap-box {
  overflow: hidden;
}
.cropper-drag-box {
  background-color: #fff;
  opacity: 0;
}
.cropper-modal {
  background-color: #000;
  opacity: 0.5;
}
.cropper-view-box {
  display: block;
  height: 100%;
  outline: 1px solid #39f;
  outline-color: rgba(51, 153, 255, 0.75);
  overflow: hidden;
  width: 100%;
}
.cropper-dashed {
  border: 0 dashed #eee;
  display: block;
  opacity: 0.5;
  position: absolute;
}
.cropper-dashed.dashed-h {
  border-bottom-width: 1px;
  border-top-width: 1px;
  height: 33.33333%;
  left: 0;
  top: 33.33333%;
  width: 100%;
}
.cropper-dashed.dashed-v {
  border-left-width: 1px;
  border-right-width: 1px;
  height: 100%;
  left: 33.33333%;
  top: 0;
  width: 33.33333%;
}
.cropper-center {
  display: block;
  height: 0;
  left: 50%;
  opacity: 0.75;
  position: absolute;
  top: 50%;
  width: 0;
}
.cropper-center:after,
.cropper-center:before {
  background-color: #eee;
  content: " ";
  display: block;
  position: absolute;
}
.cropper-center:before {
  height: 1px;
  left: -3px;
  top: 0;
  width: 7px;
}
.cropper-center:after {
  height: 7px;
  left: 0;
  top: -3px;
  width: 1px;
}
.cropper-face,
.cropper-line,
.cropper-point {
  display: block;
  height: 100%;
  opacity: 0.1;
  position: absolute;
  width: 100%;
}
.cropper-face {
  background-color: #fff;
  left: 0;
  top: 0;
}
.cropper-line {
  background-color: #39f;
}
.cropper-line.line-e {
  cursor: ew-resize;
  right: -3px;
  top: 0;
  width: 5px;
}
.cropper-line.line-n {
  cursor: ns-resize;
  height: 5px;
  left: 0;
  top: -3px;
}
.cropper-line.line-w {
  cursor: ew-resize;
  left: -3px;
  top: 0;
  width: 5px;
}
.cropper-line.line-s {
  bottom: -3px;
  cursor: ns-resize;
  height: 5px;
  left: 0;
}
.cropper-point {
  background-color: #39f;
  height: 5px;
  opacity: 0.75;
  width: 5px;
}
.cropper-point.point-e {
  cursor: ew-resize;
  margin-top: -3px;
  right: -3px;
  top: 50%;
}
.cropper-point.point-n {
  cursor: ns-resize;
  left: 50%;
  margin-left: -3px;
  top: -3px;
}
.cropper-point.point-w {
  cursor: ew-resize;
  left: -3px;
  margin-top: -3px;
  top: 50%;
}
.cropper-point.point-s {
  bottom: -3px;
  cursor: s-resize;
  left: 50%;
  margin-left: -3px;
}
.cropper-point.point-ne {
  cursor: nesw-resize;
  right: -3px;
  top: -3px;
}
.cropper-point.point-nw {
  cursor: nwse-resize;
  left: -3px;
  top: -3px;
}
.cropper-point.point-sw {
  bottom: -3px;
  cursor: nesw-resize;
  left: -3px;
}
.cropper-point.point-se {
  bottom: -3px;
  cursor: nwse-resize;
  height: 20px;
  opacity: 1;
  right: -3px;
  width: 20px;
}
@media (min-width: 768px) {
  .cropper-point.point-se {
    height: 15px;
    width: 15px;
  }
}
@media (min-width: 992px) {
  .cropper-point.point-se {
    height: 10px;
    width: 10px;
  }
}
@media (min-width: 1200px) {
  .cropper-point.point-se {
    height: 5px;
    opacity: 0.75;
    width: 5px;
  }
}
.cropper-point.point-se:before {
  background-color: #39f;
  bottom: -50%;
  content: " ";
  display: block;
  height: 200%;
  opacity: 0;
  position: absolute;
  right: -50%;
  width: 200%;
}
.cropper-invisible {
  opacity: 0;
}
.cropper-bg {
  background-image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQAQMAAAAlPW0iAAAAA3NCSVQICAjb4U/gAAAABlBMVEXMzMz////TjRV2AAAACXBIWXMAAArrAAAK6wGCiw1aAAAAHHRFWHRTb2Z0d2FyZQBBZG9iZSBGaXJld29ya3MgQ1M26LyyjAAAABFJREFUCJlj+M/AgBVhF/0PAH6/D/HkDxOGAAAAAElFTkSuQmCC");
}
.cropper-hide {
  display: block;
  height: 0;
  position: absolute;
  width: 0;
}
.cropper-hidden {
  display: none !important;
}
.cropper-move {
  cursor: move;
}
.cropper-crop {
  cursor: crosshair;
}
.cropper-disabled .cropper-drag-box,
.cropper-disabled .cropper-face,
.cropper-disabled .cropper-line,
.cropper-disabled .cropper-point {
  cursor: not-allowed;
}

`;

window.requestAnimationFrame(() => {
  document.body.appendChild(script);
  document.body.appendChild(cropperStyle);
})

let initCropBoxLeft = undefined;
let initCropBoxTop = undefined;

function setInitCropPosition(x, y) {
  initCropBoxLeft = x;
  initCropBoxTop = y;
  console.log(x, y);
}

function resetInitCropPosition() {
  initCropBoxLeft = undefined;
  initCropBoxTop = undefined;
}

document.addEventListener("contextmenu", e => {
  setInitCropPosition(e.pageX, e.pageY);
})

document.addEventListener('handleScreenshot', e => {

  const image = new Image();
  image.src = e.detail.base64;
  Object.assign(image.style, {
    width: "100vw",
    height: "100vh",
    display: 'block',
    'max-width': '100%',
  });
  document.body.appendChild(image);

  // eslint-disable-next-line no-undef
  const cropper = new Cropper(image, {
    dragBoxClass: 'block',
    initialHeight: 4,
    initialWidth: 400,
    /**
    * 下面四个全是修改源代码 直接设置裁剪框的样式
    */
    initCropBoxLeft,
    initCropBoxTop,
    initCropBoxWidth: 400,
    initCropBoxHeight: 80,
    minCropBoxHeight: 15,
    minCanvasWidth: innerWidth,
    maxCanvasWidth: innerWidth,
    minCanvasHeight: innerHeight,
    maxCanvasHeight: innerHeight,
    width: 300,					//裁剪框最大宽度  不会大于裁剪区域宽度
    height: innerHeight,					//裁剪框最大高度  不会大于裁剪区域高度
    encode: 'base64',				//文件类型
    center: false,
    type: 'png',					//保存图片类型
    name: 'img',
    quality: 1,
    scalable: false,
    // background: false,
    zoomable: false,
    onDone: function (e) {
      //裁剪完成
      console.log(e);
    },
    onCancel: function () {
      //取消裁剪
    }
  });

  image.addEventListener('ready', () => {
    console.log(cropper);
    cropper.moveTo(100, 100);
  })

  const myStyle = document.createElement('style');
  myStyle.innerHTML = `
        .cropper-container{
            position:fixed !important;
            top:0;
            left:0;
            z-index:9999;
        }
        `;
  document.head.appendChild(myStyle);

  //截取操作面板
  const clipBtn = createButton('识别', 'fixedBtn');
  const close = createButton('结束', 'fixedBtn');
  clipBtn.style['left'] = '45vw';
  close.style['left'] = '65vw';
  document.body.appendChild(clipBtn);
  document.body.appendChild(close);

  clipBtn.onclick = () => {
    const base = cropper.getCroppedCanvas().toDataURL();
    const image = new Image();
    image.src = base;
    document.body.appendChild(image);
    const event = new CustomEvent('ocrScreenshot', { detail: { image } })
    window.top.document.dispatchEvent(event);
    resetInitCropPosition();
  }

  close.onclick = () => {
    cropper.cropper.parentElement.removeChild(cropper.cropper)
    document.body.removeChild(clipBtn);
    document.body.removeChild(close);
  }

})


/* eslint-disable no-inner-declarations */
/*
 * @Author: hongbin
 * @Date: 2022-05-13 10:27:23
 * @LastEditors: hongbin
 * @LastEditTime: 2022-05-17 13:36:34
 * @Description: 识别题目文字 进行多个题库搜索 显示答案
 */

/**
* 学习通有多层  iframe 嵌套 只在一层或当前iframe层执不行
* 些许操作需挂在到最高处window上 iframe调用window.top的方法来实现 只有一个搜索框
* 需要处理后出现的iframe的监听
*/

const log = window.top.console.log;

/**
* 只在第一个window 也就是最高层执行
*/
if (!window.top.haveOnes) {
  window.top.haveOnes = true;

  /**
  * 样式前缀 避免重名
  */
  const stylePrefix = 'hongbin666-';
  const warn = window.top.console.warn;
  // const ableElements = ['P', 'SPAN', 'DIV'];

  function createText(text, className) {
    const span = createDom('span', className);
    span.innerHTML = text;
    return span;
  }

  const style = createDom('style');
  style.innerHTML = `
    .${stylePrefix}container img {
        box-shadow: 2px 3px 2px 0 #a9a9a9, -2px -1px 2px 0px #e7e7e7;
        margin: 0.5vh;
        border-radius: 0.2vw;
        max-height:100px;
    }
      ::-webkit-scrollbar-thumb {
        background: #000;
        border-radius: 4px;
      }
      ::-webkit-scrollbar {
        width: 4px;
        height: 4px;
        background-color: rgba(1, 1, 1, 0);
      }
      .${stylePrefix}container{
        position: fixed;
        z-index:9999999999;
        width: 6.2vw;
        minWidth: 60px;
        height: 2.5vh;
        min-width: 25px;
        background: linear-gradient(45deg, #ffffff, #f4f4f4);
        border-radius: 1vmax;
        box-shadow:inset -4px -4px 8px 0px #cfcfcf, inset 5px 4px 6px 0px #e3e3e3;
        top: 20vh;
        right: 5vw;
        padding: 1vh 1vw;
        transition: width .3s linear,height .3s linear;
      }
      .${stylePrefix}innerContainer{
        overflow: scroll;
        width:100%;
        height:100%;
        transition: all .3s linear;
        transition-property: opacity,visibility;
      }
      .${stylePrefix}caption{
        font-size: 0.9vw;
        letter-spacing: 1px;
        font-weight: bold;
        background: linear-gradient(64deg, #2417f1, #8a0bed);
        padding: 0.2vh 0.2vw;
        color: #fff;
        border-radius: 0.2vw;
        display:block;
        margin-bottom:0.2vh;
        width: 5em;
        text-align: center;
      }
      .${stylePrefix}answer{
        font-size: 1vw;
        letter-spacing: 1px;
        color: #ffffff;
        background: #000000;
        padding:0.5vh;
        border-radius:0.5vh;
        margin:0.5vh;
        display:block;
      }
      .${stylePrefix}btn{
        margin-top: 1vh;
        margin-bottom: 1vh;
        margin-right: 1vw;
        transition: all 0.3s linear;
        padding: 4px 10px;
        border: none;
        background: linear-gradient(45deg, #35336c, #544d61);
        color: #fffae5;
        border-radius: 5px;
        box-shadow: 8px 4px 10px 3px #ccc;
        cursor: pointer;
        font-weight: bold;
      }
      .${stylePrefix}fixedBtn{
        transition: all 0.3s linear;
        padding: 14px 20px;
        border: none;
        font-size:2vw;
        background: linear-gradient(45deg, #0219f2, #c804ea);
        color: #fffae5;
        border-radius: 5px;
        box-shadow: 0px 1px 0px 5px #ccc;
        cursor: pointer;
        font-weight: bold;
        position:fixed;
        bottom: 2vh;
        z-index:9999;
      }
      .${stylePrefix}fixedBtn:hover{
        filter: contrast(0.6);
      }
      
      .${stylePrefix}hideBtn,.${stylePrefix}move{
          position:absolute;
          top:0.8vh;
          right:5vmax;
          transition: all 0.3s linear;
          padding: 4px 10px;
          border: none;
          background: linear-gradient(45deg, #35336c, #544d61);
          color: #fffae5;
          border-radius: 5px;
          box-shadow: 8px 4px 10px 3px #ccc;
          cursor: pointer;
          font-weight: bold;
          font-size: 1vw;
      }
      .${stylePrefix}move{
        right:0.5vmax;
        cursor: move;
        box-shadow: 2px 3px 10px 3px #bebebe;
      }
      .${stylePrefix}tipText{
        color: #ffffff;
        letter-spacing: 2px;
        text-align: center;
        margin-top: 23vh;
        display: block;
        text-shadow: 6px 2px 7px #6c6a6a;
        -webkit-text-stroke: 1px #ffffff;
      }
    `;
  window.requestAnimationFrame(() => {
    console.log(document.head);
    document.head.appendChild(style);
  })

  function setStyle(dom, styles) {
    for (const [name, val] of Object.entries(styles)) {
      dom.style[name] = val;
    }
  }

  /**
  * 渲染控制器
  */
  const control = {
    list: [],
    container: createDom('div', 'innerContainer'),
    length: function () { return this.list.length },
    newElement: function (canvas) {
      this.list.push({ canvas });
      this.render();
    },
    setTexts: function (index, texts) {
      this.list[index].texts = texts;
      this.render();
    },
    render: function () {
      log(this.list);
      this.container.innerHTML = '';
      this.list.forEach(({ canvas, texts = [] }) => {
        this.container.appendChild(createText('点击题目', 'caption'));
        this.container.appendChild(canvas);

        if (texts === '识别文本中...') this.container.appendChild(createText(texts, 'caption'));

        Array.isArray(texts) && texts.forEach(({ words: text }) => {
          this.container.appendChild(createText('识别文本', 'caption'));
          this.container.appendChild(createText(text, 'answer'));
          this.container.appendChild(createButton('复制', 'btn', () => {
            const isCopySuccess = copyToClipboard(text);
            if (isCopySuccess) {
              log("复制成功:", text);
            }
          }));
          this.container.appendChild(createButton('复制选中内容', 'btn', () => {
            const selectText = getSelection().toString().trim().replace(/\n/g, "");
            if (!selectText) return console.warn('未获取选中内容');
            const isCopySuccess = copyToClipboard(selectText);
            if (isCopySuccess) {
              log("复制成功:", selectText);
            }

          }));
          this.container.appendChild(createButton('搜题', 'btn', () => {
            log(text);
          }));
        })
        const hr = createDom('hr');
        hr.style['margin-top'] = '1vh';
        this.container.appendChild(hr)
      });
    },
    init: function () {
      const wrap = createDom('div', 'container');
      window.requestAnimationFrame(() => {
        document.body.appendChild(wrap);
      })
      wrap.appendChild(this.container);
      const hideBtn = createButton('放', 'hideBtn', e => {
        const hide = e.target.innerHTML === "收";
        e.target.innerHTML = hide ? '放' : "收";
        setStyle(wrap, {
          width: hide ? '6.2vw' : "30vw",
          minWidth: hide ? '60px' : "400px",
          height: hide ? '2.5vh' : "50vh",
        })
        setStyle(wrap.firstChild, {
          visibility: hide ? 'hidden' : "visible", opacity: hide ? 0 : 1,
          overflow: hide ? 'hidden' : "",
        })
      });
      wrap.appendChild(hideBtn);
      const moveBtn = createButton('移动', 'move');
      wrap.appendChild(moveBtn);
      const tip = createText('右键点击插件使用', 'tipText');
      this.container.appendChild(tip);
      moveBtn.addEventListener('mousedown', (e) => {
        const { pageX, pageY } = e;
        const wrap = document.querySelector('.hongbin666-container');
        const { offsetLeft, offsetTop, offsetWidth } = wrap;
        const prevRight = window.innerWidth - offsetLeft - offsetWidth;

        const move = e => {
          const disX = e.pageX - pageX;
          const disY = e.pageY - pageY;
          const top = offsetTop + disY;
          const right = prevRight - disX;
          if (top > 10 && top < window.innerHeight - 80) {
            wrap.style['top'] = top + 'px';
          }
          if (right > 30 && right < window.innerWidth - 60) {
            wrap.style['right'] = right + 'px';
          }
        }
        const clearListen = () => {
          document.removeEventListener('mousemove', move)
          document.removeEventListener('mouseup', clearListen)
          document.removeEventListener('mouseleave', clearListen)
        }
        document.addEventListener('mousemove', move);
        document.addEventListener('mouseup', clearListen)
        document.addEventListener('mouseleave', clearListen)
      })
    }
  }

  control.init();
  /**
  * 监听到识别操作 发送请求识别文字
  */
  document.addEventListener('ocrScreenshot', (e) => {
    const element = e.detail.image;
    const base64 = e.detail.image.src;
    const length = control.length();
    control.newElement(element);
    control.setTexts(length, '识别文本中...');

    textDistinguish(base64, (res) => {
      if (res.words_result) {
        log(res);
        control.setTexts(length, res.words_result);
      }
    })
  })

  // const baseURL = 'http://localhost:4000/ocr';
  const baseURL = 'https://hongbin.xyz:4000/ocr';
  /**
  * 发送请求获取文字
  */
  function textDistinguish(image, callback) {
    fetch(baseURL, {
      method: "post",
      headers: {
        'Content-Type': "application/x-www-form-urlencoded",
      },
      body: JSON.stringify({ image, free: false })
    }).then(res => res.json())
      .then(callback)
      .catch(err => {
        console.error('err', err);
      });
  }

  /**
  * 将点击的内容绘制到canvas生成图片
  */
  async function drawCanvas(target) {
    // eslint-disable-next-line no-undef
    const element = await html2canvas(target);
    if (element) {
      //去掉头部格式
      // const base64 = element.toDataURL("image/png").substr(22);
      const base64 = element.toDataURL("image/png");
      return { element, base64 };
    }
  }

  function copyToClipboard(text) {
    if (window.clipboardData && window.clipboardData.setData) {
      //@ts-ignore
      return window.clipboardData.setData("Text", text);
    } else if (document.queryCommandSupported && document.queryCommandSupported("copy")) {
      var textarea = document.createElement("textarea");
      textarea.textContent = text;
      textarea.style.position = "fixed"; // Prevent scrolling to bottom of page in Microsoft Edge.
      document.body.appendChild(textarea);
      textarea.select();
      try {
        return document.execCommand("copy"); // Security exception may be thrown by some browsers.
      } catch (ex) {
        warn("Copy to clipboard failed.", ex);
        return false;
      } finally {
        document.body.removeChild(textarea);
      }
    }
  }

} else {
  log('iframe:', window.location.pathname);
}
